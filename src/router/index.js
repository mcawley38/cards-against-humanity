import Vue from 'vue'
import VueRouter from 'vue-router'
import Register from '../views/Register.vue'
import Dashboard from '../views/Dashboard.vue'
import Game from '../views/Game.vue'
import axios from 'axios'

Vue.use(VueRouter)

const routes = [
    {
        path: '/register',
        name: 'Register',
        component: Register
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard
    },
    {
        path: '/game',
        name: 'Game',
        component: Game
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach(async (to, from, next) => {
    const userId = localStorage.getItem('userId')
    const secret = localStorage.getItem('secret')

    if (userId && secret) {
        await axios.get('http://localhost:4000/users/' + userId + '/' + secret)
            .then(function (response) {
                response.data.data.gameId && localStorage.setItem('gameId', response.data.data.gameId)

                next(
                    response.data.data.gameId
                    ? to.name !== 'Game'
                      ? {name: 'Game'}
                      : {}
                    : to.name !== 'Dashboard'
                      ? {name: 'Dashboard'}
                      : {}
                )
            })
            .catch(function () {
                localStorage.removeItem('userId')
                localStorage.removeItem('secret')
                localStorage.removeItem('gameId')

                next(to.name === 'Register' ? {} : {name: 'Register'})
            })
    } else {
        next(to.name === 'Register' ? {} : {name: 'Register'})
    }
})

export default router
