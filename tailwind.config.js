module.exports = {
    theme: {
        extend: {
            spacing: {
                '500': '500px',
            },
            inset: {
                '1/2': '50%',
            },
            fontSize: {
                '2xs': '0.5rem',
            }
        },
        variants: {},
        plugins: [],
    }
}