module.exports = function (app: any) {
    require('./UserRoutes')(app);
    require('./GameRoutes')(app);
};