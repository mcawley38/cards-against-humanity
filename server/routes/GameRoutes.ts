import {Game} from "../classes/game/Game";
import {User} from "../classes/user/User";

const validate = require('../validations/Validation');

module.exports = function (app: any) {

    app.get('/games', function (req: any, res: any) {
        res.sendJsend(200, 'success', {games: app.globals.games.filter(req.query)});
    });

    app.post('/games/join/:gameId', function (req: any, res: any) {
        const user = app.globals.users.getUser(req.body.user.id, req.body.user.secret);

        !(user instanceof User) && res.sendJsend(422, 'error', {'errors': [{field: 'user', msg: 'user is invalid'}]});

        const game = app.globals.games.games[req.body.game.id];

        if (game instanceof Game) {
            !game.config.password || game.config.password === req.body.game.password
                ? (
                    game.addUser(user)
                        ? res.sendJsend(200, 'success', {})
                        : res.sendJsend(200, 'error', {'errors': [{field: 'game', msg: 'game is full'}]})
                )
                : res.sendJsend(422, 'error', {'errors': [{field: 'password', msg: 'password is invalid'}]});
        } else {
            res.sendJsend(400, 'error', {'errors': [{field: 'game', msg: 'game is invalid'}]});
        }
    });

    app.post('/games', require('../validations/game/GameCreateValidation')(), validate, function (req: any, res: any) {
        const host = app.globals.users.getUser(req.body.host, req.body.secret);

        if (!host) {
            res.sendJsend(422, 'error', {'errors': [{field: 'host', msg: 'host is invalid'}]});
        }

        const game = app.globals.games.addGame(req.body.name, host, req.body.config);

        if (game instanceof Game) {
            host.assignGame(game);

            res.sendJsend(200, 'success', {
                game: {
                    id: game.id,
                    password: game.config.password
                }
            });
        } else {
            res.sendJsend(422, 'error', {
                errors: [
                    {
                        field: 'name',
                        msg: 'That game name is already in use, please chose another'
                    }
                ]
            });
        }
    });

    // app.post('/game/:gameId/', function (req: any, res: any) {
    //     const host = app.globals.users.getUser(req.body.host,req.body.secret);
    //
    //     if (!(host instanceof User)) {
    //         res.sendJsend(422, 'error', {'errors':[{field:'host',msg:'host is invalid'}]});
    //     }
    //
    //     const game = app.globals.games.addGame(req.body.name,host,req.body.config);
    //
    //     if (game instanceof Game) {
    //         host.assignGame(game);
    //
    //         res.sendJsend(200, 'success', {
    //             game: {
    //                 id:game.id,
    //                 password: game.config.password
    //             }
    //         });
    //     } else {
    //         res.sendJsend(422, 'error', {
    //             errors: [
    //                 {
    //                     field: 'name',
    //                     msg: 'That game name is already in use, please chose another'
    //                 }
    //             ]
    //         });
    //     }
    // });
};