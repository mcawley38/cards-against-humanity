export {};

const {body} = require('express-validator');

module.exports = function () {
    return [
        body('host')
            .isString(),
        body('name')
            .isLength({min: 3})
            .withMessage('The player name field must be at least 3 characters'),
        body('name')
            .isLength({max: 10})
            .withMessage('The player name field may not be greater than 10 characters'),
        body('config.password')
            .isLength({max: 10})
            .withMessage('The password field may not be greater than 10 characters'),
        body('config.max_score')
            .isIn([
                0,
                5,
                10,
                15,
                20
            ]),
        body('config.max_players')
            .isIn([
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10
            ]),
        body('config.round_time_limit')
            .isIn([
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15,
                16,
                17,
                18,
                19,
                20
            ]),
        body('config.game_time_limit')
            .isIn([
                0,
                20,
                10,
                30,
                40,
                50,
                60,
                70,
                80,
                90,
                100,
                110,
                120
            ])
    ]
};