const {validationResult} = require('express-validator');

const validate = (req: any, res: any, next: any) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        return next()
    }
    const extractedErrors: Array<object> = [];
    errors.array().map(function (err: any) {
        return extractedErrors.push({field: err.param, msg: err.msg})
    });

    return res.sendJsend(422, 'error', {errors: extractedErrors,});
};

module.exports = validate;