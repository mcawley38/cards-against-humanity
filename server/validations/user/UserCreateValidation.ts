export {};

const {body} = require('express-validator');

module.exports = function () {
    return [
        body('name')
            .isLength({min: 3})
            .withMessage('The player name field must be at least 3 characters'),
        body('name')
            .isLength({max: 10})
            .withMessage('The player name field may not be greater than 10 characters'),
    ]
};