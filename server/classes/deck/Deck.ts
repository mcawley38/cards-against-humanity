import {Card} from "./cards/Card";
import {WhiteCard} from "./cards/WhiteCard";
import {BlackCard} from "./cards/BlackCard";
import {Helper} from "../services/Helper";

type card_status = ("used" | "inPlay" | "inDeck");

export class Deck {
    cards: Array<Card> = [];
    isWhiteDeck: boolean;

    constructor(isWhiteDeck: boolean) {
        this.isWhiteDeck = isWhiteDeck;
        this.build();
    }

    private build() {
        const cardsConfig: Array<string> = this.isWhiteDeck
            ? require("../../../config/decks/whiteCards.json")
            : require("../../../config/decks/blackCards.json");

        Helper.shuffle(cardsConfig);

        let self = this;

        cardsConfig.forEach(function (description, index) {
            self.cards[index] = self.isWhiteDeck
                ? new WhiteCard(index, description)
                : new BlackCard(index, description);
        });
    }

    getByStatus(status: card_status, change_status: card_status, limit: number = 1) {
        const cards: Array<Card> = [];
        let i = 0;

        for (const card of this.cards) {
            if (card.status === status) {
                card.status = change_status;
                cards.push(card);
                i++;

                if (cards.length === limit) {
                    return cards;
                }
            }
        }

        return this.cards.filter((card) => {
            return card.status = status
        })
    }
}