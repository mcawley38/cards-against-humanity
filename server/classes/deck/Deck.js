import { WhiteCard } from "@/classes/cards/WhiteCard";
import { BlackCard } from "@/classes/cards/BlackCard";
import { Helper } from "@/classes/Helper";
export class Deck {
    constructor(isWhiteDeck) {
        this.cards = [];
        this.isWhiteDeck = isWhiteDeck;
        this.build();
    }
    draw(amount = 1) {
        const drawnCards = [];
        for (const card of this.cards) {
            if (card.status === "inDeck") {
                card.status = "inPlay";
                drawnCards.push(card);
                if (drawnCards.length === amount) {
                    break;
                }
            }
        }
        return drawnCards;
    }
    async build() {
        const cardsConfig = this.isWhiteDeck
            ? await require("../../../config/decks/whiteCards.json")
            : await require("../../../config/decks/blackCards.json");
        Helper.shuffle(cardsConfig);
        let i = 1;
        for (const description of cardsConfig) {
            this.cards[i] = this.isWhiteDeck
                ? new WhiteCard(i, description)
                : new BlackCard(i, description, (description.match(/_/g) || ["_"]).length);
            i++;
        }
    }
    getByStatus(status) {
        return this.cards.filter((card) => {
            return card.status = status;
        });
    }
}
//# sourceMappingURL=Deck.js.map