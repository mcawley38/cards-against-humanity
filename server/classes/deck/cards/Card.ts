export class Card {
    id: number;
    description: string;
    status: ("used" | "inPlay" | "inHand" | "inDeck");
    isWhite: boolean;

    constructor(id: number, description: string, isWhite: boolean) {
        this.id = id;
        this.description = description;
        this.status = "inDeck";
        this.isWhite = isWhite;
    }
}

