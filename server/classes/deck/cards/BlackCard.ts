import {Card} from "./Card";

export class BlackCard extends Card {
    valuesCount: number;

    constructor(id: number, description: string) {
        super(id, description, false);
        this.valuesCount = (description.match(/_/g) || []).length || 1;
    }
}