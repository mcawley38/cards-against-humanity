import { Card } from "@/classes/cards/Card";
export class BlackCard extends Card {
    constructor(id, description, valueCount) {
        super(id, description, false);
        this.valuesCount = valueCount;
    }
}
//# sourceMappingURL=BlackCard.js.map