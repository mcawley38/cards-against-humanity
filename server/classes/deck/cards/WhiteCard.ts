import {Card} from "./Card";

export class WhiteCard extends Card {
    constructor(id: number, description: string) {
        super(id, description, true)
    }
}