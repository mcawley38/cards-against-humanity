export class Card {
    constructor(id, description, isWhite) {
        this.id = id;
        this.description = description;
        this.status = "inDeck";
        this.isWhite = isWhite;
    }
}
//# sourceMappingURL=Card.js.map