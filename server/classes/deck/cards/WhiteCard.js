import { Card } from "@/classes/cards/Card";
export class WhiteCard extends Card {
    constructor(id, description) {
        super(id, description, true);
    }
}
//# sourceMappingURL=WhiteCard.js.map