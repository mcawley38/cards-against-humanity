export class GameData {
    current_game?: string = null;
    score?: number = null;
    taken_turn:boolean = false;
    is_czar: boolean = false;
}