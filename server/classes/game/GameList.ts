import {Game} from "./Game";
import {User} from "../user/User";
import {GameConfig} from "../../interfaces/GameConfig";

export class GameList {
    games: { [key: string]: Game } = {};
    io: any;

    constructor(io: any) {
        this.io = io;
    }

    public getGame(id: string, password: string): Game | false {
        let game = this.games[id];

        return (game && (game.config.password ? game.config.password === password : true))
            ? game
            : false;
    }

    public addGame(name: string, host: User, config: GameConfig): false | Game {
        const safeName = name.trim();
        const id = Game.generateNewId(safeName);

        if (!this.games[id]) {
            const game = new Game(id, this.io.of('/'+id), safeName, host, config);
            this.games[id] = game;

            return game
        } else {
            return false
        }
    }

    filter(filters: any): Array<object> {
        let games: Array<object> = [];
        for (let gameId in this.games) {
            const game: Game = this.games[gameId];

            if (filters.name && !game.name.includes(filters.name)) {
                continue
            }

            if (filters.private === 'true' && !game.config.password) {
                continue
            }

            if (filters.only_open == 'true' && game.members.length >= game.config.max_players) {
                continue
            }

            if (
                filters.player_count
                && !isNaN(filters.player_count)
                && game.members.length
                && game.members.length <= parseInt(filters.player_count)
            ) {
                continue
            }

            games.push({
                name: game.name,
                id: game.id,
                private: !!game.config.password,
                player_count: game.members.length,
                max_players: game.config.max_players,
                max_score: game.config.max_score,
            })
        }

        return games;
    }
}