import { Deck } from "@/classes/arrays/Deck";
export class Game {
    constructor(id, name, host) {
        this.members = [];
        this.config = {
        // password?: string;
        // max_score?: number;
        // max_players?: number;
        // round_time_limit?: number;
        // game_time_limit?: number;
        };
        this.id = id;
        this.name = name;
        this.host_id = host.id;
        this.members.push(host);
        // this.members = {};
        this.deck = new Deck(false);
    }
}
//# sourceMappingURL=Game.js.map