import { User } from "@/classes/User";
export class UserList {
    constructor() {
        this.users = {};
    }
    addUser(name) {
        if (!this.checkName(name)) {
            const id = User.generateNewId(name);
            const user = new User(id, name);
            this.users[id] = user;
            return user;
        }
        else {
            return false;
        }
    }
    checkName(name) {
        return !!this.users[User.generateNewId(name)];
    }
    getUser(id) {
        return this.users[id] || false;
    }
}
//# sourceMappingURL=UserList.jsap