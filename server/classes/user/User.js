import { Deck } from "@/classes/arrays/Deck";
import { Md5 } from "ts-md5";
export class User {
    // salt = 'A1*3imKx;';
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
    assignDeck() {
        this.deck = new Deck(true);
    }
    getHand() {
        return this.deck
            ? this.deck.getByStatus('inPlay')
            : [];
    }
    static generateNewId(name) {
        return Md5.hashStr(name);
    }
}
//# sourceMappingURL=User.js.map