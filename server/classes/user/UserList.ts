import {User} from "./User";

export class UserList {
    users: { [key: string]: User } = {};

    public addUser(name: string): false | User {
        const safeName = name.trim();
        const id = User.generateNewId(safeName);

        if (!this.users[id]) {
            const user = new User(id, safeName);
            this.users[id] = user;

            return user
        } else {
            return false
        }
    }

    public getUser(id: string, secret: string) {
        const user = this.users[id];

        return (user && user.secret === secret)
            ? user
            : false;
    }
}