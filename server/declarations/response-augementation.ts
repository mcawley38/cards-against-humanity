import {Response} from "express-serve-static-core";
import {response} from "express";

type httpCode = 200 | 400 | 422 | 403 | 500;
type status = 'success' | 'fail' | 'error';

declare module "express-serve-static-core" {
    export interface Response {
        sendJsend(httpCode: httpCode, status: status, data: object): void;
    }
}

// sends a JSend formatted response
response.sendJsend = function (httpCode: httpCode, status: status, data: object) {
    this.type('json');
    this.status(httpCode);
    this.json({
        status: status,
        data: data
    });
};