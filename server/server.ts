import {UserList} from "./classes/user/UserList";
import {GameList} from "./classes/game/GameList";

import './declarations/response-augementation';
import {User} from "@/classes/user/User";
import {GameConfig} from "@/interfaces/GameConfig";

const express = require("express");
const app = express();

const cors = require('cors');
const corsConfig = require('../config/server/cors.json');
const port = 4000;

app.use(cors(corsConfig));
app.use(express.json());

const server = app.listen(`${port}`, function () {
    console.log(`Server started on port ${port}`);
});

const io = require("socket.io")(server);

app.globals = {
    users: new UserList(),
    games: new GameList(io)
};

require('./routes/index')(app);

app.globals.games.addGame('password', app.globals.users.addUser('test'),
    {
        password: 'password',
        max_score: 10000,
        max_players: 10,
        round_time_limit: 0,
        game_time_limit: 0
    });

app.globals.games.addGame('no password', app.globals.users.addUser('test1'),
    {
        password: null,
        max_score: 10000,
        max_players: 10,
        round_time_limit: 0,
        game_time_limit: 0
    });

app.globals.games.addGame('name2', app.globals.users.addUser('test2'),
    {
        password: 'password',
        max_score: 10000,
        max_players: 1,
        round_time_limit: 0,
        game_time_limit: 0
    });

// console.log(app.globals.users);
// console.log(app.globals.games);

// io.on("connection", function (socket: any) {
//     // console.log('connected',socket.id);
//
//
//
//     socket.on('verify', function (data: any) {
//         let user = app.globals.users.getUser(data.id, data.secret);
//         let game = app.globals.games[user.game_data.current_game];
//
//         if (user && user.game && io.sockets.adapter.rooms[game.id]) {
//             user.session_id = socket.id;
//             // socket.join(game.id);
//             socket.join('test');
//         } else {
//             socket.disconnect();
//         }
//     });
// });