export interface GameConfig {
    password?: string,
    max_score?: number,
    max_players?: number,
    round_time_limit?: number,
    game_time_limit?: number
}