# cards-against-humanity

## Project setup
```
npm install
```

### Compiles and hot-reloads frontend for development
```
npm run vue-server
```

### Compiles typescript and runs node server for development
```
npm run node-server
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

#NOTES

###JSEND
this project is using JSend format for api endpoints [docs](https://https://github.com/omniti-labs/jsend).

### readme
currently this file is rather scarce, if you feel anything is missing or needed etc feel free to add to this file